import React from "react";
import ReactDOM from "react-dom";
import { Catalog, pageLoader } from "catalog";

const theme = {
  fontFamily: 'Open Sans, sans-serif',
  fontHeading: 'Montserrat, sans-serif',
  lightColor: '#D6D6D6',
  pageHeadingBackground: '#78AD20',
  pageHeadingTextColor: '#fff',
  navBarBackground: '#F2F2F2',
  navBarTextColor: '#004c42',
  brandColor: '#78AD20',
  sidebarColor: '#FFFFFF',
  sidebarColorActive: '#D1312E',
  sidebarColorText: '#004c42',
  sidebarColorTextActive: '#78AD20',
  sidebarColorLine: '#EBEBEB',
  sidebarColorHeading: '#003B5C',
  bgLight: '#F2F2F2',
  bgDark: '#333333',
};

const pages = [
  {
    title: 'Introduction',
    pages: [
      {
        path: '/',
        title: 'The Recycle My Electronics National Brand',
        content: pageLoader(() => import('./intro.md'))
      },
      {
        path: '/applying-the-modular-approach',
        title: 'Applying The Modular Approach To The National Campaign',
        content: pageLoader(() => import('./applying-the-modular-approach.md'))
      },
      {
        path: '/more-information',
        title: 'For More Information About How To Best Apply This Brand',
        content: pageLoader(() => import('./more-information.md'))
      },
    ]
  },

  {
    title: 'Logo Sets & Usage',
    pages: [
      {
        path: '/logo/primary-consumer',
        title: 'Primary Consumer',
        content: pageLoader(() => import('./logo/primary-consumer.md'))
      },
      {
        path: '/logo/secondary-stacked-consumer',
        title: 'Secondary Stacked Consumer',
        content: pageLoader(() => import('./logo/secondary-stacked-consumer.md'))
      },
      {
        path: '/logo/secondary-horizontal-consumer',
        title: 'Secondary Horizontal Consumer',
        content: pageLoader(() => import('./logo/secondary-horizontal-consumer.md'))
      },
      {
        path: '/logo/primary-operations',
        title: 'Primary Operations',
        content: pageLoader(() => import('./logo/primary-operations.md'))
      },
      {
        path: '/logo/primary-b2b',
        title: 'Primary Business-to-business',
        content: pageLoader(() => import('./logo/primary-b2b.md'))
      },
      {
        path: '/logo/primary-consumer-usage',
        title: 'Primary Consumer Logo Usage',
        content: pageLoader(() => import('./logo/primary-consumer-usage.md'))
      },
      {
        path: '/logo/corporate-colours',
        title: 'Corporate Colours',
        content: pageLoader(() => import('./logo/corporate-colours.md'))
      },
      {
        path: '/logo/corporate-fonts',
        title: 'Corporate Fonts',
        content: pageLoader(() => import('./logo/corporate-fonts.md'))
      },
    ]
  },
  {
    title: 'Guidelines',
    pages: [
      {
        path: '/guidelines/future-is-in-your-hands',
        title: 'The Future Is In Your Hands. Don\'t Let It Go To Waste.',
        content: pageLoader(() => import('./guidelines/future-is-in-your-hands.md'))
      },
      {
        path: '/guidelines/minimum-size',
        title: 'Minimum Size',
        content: pageLoader(() => import('./guidelines/minimum-size.md'))
      },
      {
        path: '/guidelines/clear-space',
        title: 'Clear Space',
        content: pageLoader(() => import('./guidelines/clear-space.md'))
      },
      {
        path: '/guidelines/incorrect-logo-usage',
        title: 'Incorrect Logo Usage',
        content: pageLoader(() => import('./guidelines/incorrect-logo-usage.md'))
      },
    ]
  },
  {
    title: 'Visual Graphic Elements',
    pages: [
      {
        path: '/visual/circuitree-watermark',
        title: 'Circuitree Maple Leaf Watermark',
        content: pageLoader(() => import('./visual/circuitree-watermark.md'))
      },
      {
        path: '/visual/circuitboard-graphics',
        title: 'Circuitboard Graphics',
        content: pageLoader(() => import('./visual/circuitboard-graphics.md'))
      },
      {
        path: '/visual/image-vignette',
        title: 'Image Vignette',
        content: pageLoader(() => import('./visual/image-vignette.md'))
      },
      {
        path: '/visual/rme-device-icons',
        title: 'RME Electronic Device Icons',
        content: pageLoader(() => import('./visual/rme-device-icons.md'))
      },
      {
        path: '/visual/text-from-tomorrow',
        title: '"Text from TOMORROW" Text Box',
        content: pageLoader(() => import('./visual/text-from-tomorrow.md'))
      },
      {
        path: '/visual/rme-heros',
        title: 'RME Heros',
        content: pageLoader(() => import('./visual/rme-heros.md'))
      },
      {
        path: '/visual/nature-background-images',
        title: 'Nature Background Images',
        content: pageLoader(() => import('./visual/nature-background-images.md'))
      },
      {
        path: '/visual/epra-logos',
        title: 'EPRA Logos',
        content: pageLoader(() => import('./visual/epra-logos.md'))
      },
      {
        path: '/visual/partner-logos',
        title: 'Partner Logos',
        content: pageLoader(() => import('./visual/partner-logos.md'))
      },
    ]
  },
  {
    title: 'Application',
    pages: [
      {
        path: '/application/rme-core-messaging',
        title: 'RME Core Messaging',
        content: pageLoader(() => import('./application/rme-core-messaging.md'))
      },
      {
        path: '/application/rme-boilerplate',
        title: 'RME Boilerplate',
        content: pageLoader(() => import('./application/rme-boilerplate.md'))
      },
      {
        path: '/application/q-and-a',
        title: 'Q&A',
        content: pageLoader(() => import('./application/q-and-a.md'))
      },
      {
        path: '/application/radio-scripts',
        title: 'Radio Scripts',
        content: pageLoader(() => import('./application/radio-scripts.md'))
      },
      {
        path: '/application/tagline-usage',
        title: 'Tagline Usage',
        content: pageLoader(() => import('./application/tagline-usage.md'))
      },
      {
        path: '/application/directing-to-website',
        title: 'Directing Our Audience To Our Website',
        content: pageLoader(() => import('./application/directing-to-website.md'))
      },
      {
        path: '/application/print-advertising',
        title: 'Print Advertising',
        content: pageLoader(() => import('./application/print-advertising.md'))
      },
      {
        path: '/application/digital-banner-ads',
        title: 'Digital Banner Ads',
        content: pageLoader(() => import('./application/digital-banner-ads.md'))
      },
      {
        path: '/application/ehf-brochures',
        title: 'EHF Brochures',
        content: pageLoader(() => import('./application/ehf-brochures.md'))
      },
      {
        path: '/application/counter-tent-cards',
        title: 'Counter/Tent Cards',
        content: pageLoader(() => import('./application/counter-tent-cards.md'))
      },
    ]
  },
  {
    path: '/contact',
    title: 'Contact',
    content: pageLoader(() => import('./contact.md'))
  }
];

ReactDOM.render(
  <Catalog 
    title='Recycle My Electronics Brand Standards'
    basePath='/rme'
    logoSrc='rme-logo.svg'
    useBrowserHistory={true}
    theme={theme}
    pages={pages} />,
  document.getElementById("catalog")
);
