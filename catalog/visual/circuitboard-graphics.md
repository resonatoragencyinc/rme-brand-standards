### Circuitboard Graphic
This circuitboard graphics element can be used behind other approved imagery in informational documents or in other graphic applications.
```image
plain: true
span: 3
src: "cboard.png"
```

```download|span-3
{
    "title": " Circuitboard Graphics White (.ai)",
    "filename": "Circuitboard Graphics White.ai",
    "url": "cboard.ai"
}
```
