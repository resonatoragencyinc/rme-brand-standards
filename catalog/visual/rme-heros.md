In unifying the Recycle My Electronics brand nationally, regional and provincial specific imagery has been used to highlight the geographical identity of each province. By marrying province specific imagery with approved Recycle My Electronics “hero” images, each province has its own unique individual character while functioning within the overarching Recycle My Electronics brand.


### Primary Hero
This is the main hero image for the National campaign.
```image
plain: true
span: 3
src: "hero1.png"
```

```download|span-3
{
    "title": " RME Primary Hero (.psd)",
    "filename": "RME PRimary Hero.psd",
    "url": "hero1.psd"
}
```


### British Columbia Alternate Hero 2
```image
plain: true
span: 3
src: "hero2.png"
```

```download|span-3
{
    "title": " BC Alternate Hero 2 (.psd)",
    "filename": "BC Alternate Hero 2.psd",
    "url": "hero2.psd"
}
```


### British Columbia Alternate Hero 3
```image
plain: true
span: 3
src: "hero3.png"
```

```download|span-3
{
    "title": " BC Alternate Hero 3 (.psd)",
    "filename": "BC Alternate Hero 3.psd",
    "url": "hero3.psd"
}
```



### Saskatchewan Alternate Hero 2
```image
plain: true
span: 3
src: "hero4.png"
```

```download|span-3
{
    "title": " SK Alternate Hero 3 (.psd)",
    "filename": "SK Alternate Hero 3.psd",
    "url": "hero4.psd"
}
```
