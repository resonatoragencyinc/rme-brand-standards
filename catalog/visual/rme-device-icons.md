### Recycle My Electronics Electronic Device Icons - (WHAT CAN BE RECYCLED?)
The what can be recycled element, featured in some advertising, is enclosed with a box element which is provided here for download. The full list of included items follows in the next section.

```image
plain: true
span: 2
src: "device1.png"
```

```download|span-1
{
    "title": " What Can Be Recycled Frame ENG POS (.ai)",
    "filename": "CWhat Can Be Recycled Frame ENG POS.ai",
    "url": "device1.ai"
}
```
```download|span-1
{
    "title": " What Can Be Recycled Frame ENG NEG (.ai)",
    "filename": "CWhat Can Be Recycled Frame ENG NEG.ai",
    "url": "device2.ai"
}
```
```download|span-1
{
    "title": " What Can Be Recycled Frame FR POS (.ai)",
    "filename": "What Can Be Recycled Frame FR POS.ai",
    "url": "device3.ai"
}
```
```download|span-1
{
    "title": " What Can Be Recycled Frame FR NEG (.ai)",
    "filename": "What Can Be Recycled Frame FR NEG.ai",
    "url": "device4.ai"
}
```




### Complete Electronic Device Icon Set - Positive
```image
plain: true
span: 3
src: "device2.png"
```

```download|span-3
{
    "title": " RME Electronic Device Icons POS (.ai)",
    "filename": "RME Electronic Device Icons POS.ai",
    "url": "device5.ai"
}
```
