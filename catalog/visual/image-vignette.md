### Image Vignette
Vignetting can be overlayed over imagery to create contrast for graphic elements and to direct the eye to highlighted content areas.
```image
plain: true
span: 3
src: "imgving.png"
```

```download|span-3
{
    "title": " Vingnette Layered SD File (.psd)",
    "filename": "Vingnette Layered SD File.psd",
    "url": "imgving.psd"
}
```
