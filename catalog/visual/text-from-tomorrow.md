### "Text from Tomorrow" Box
In many instances in advertising, the lead character in each piece is receiving a "text from: TOMORROW". This element can be found for download here in both French and English.

```image
plain: true
span: 2
src: "text.png"
```

```download|span-2
{
    "title": " Text From Tomorrow Eng(.ai)",
    "filename": "Text From Tomorrow Eng.ai",
    "url": "text_en.ai"
}
```

```download|span-2
{
    "title": " Text From Tomorrow Fr (.ai)",
    "filename": "Text From Tomorrow Fr.ai",
    "url": "text_fr.ai"
}
```
