In each province, there are variations of logo treatment and partner logos and elements that need to be showcased. These are available for download here.

### Return It Electronics Logo
```image
plain: true
span: 2
src: "returnit.png"
```

```download|span-2
{
    "title": "Return It Electronics POS(.ai)",
    "filename": "Return It Electronics POS.ai",
    "url": "returnit_colour.ai"
}
```

```download|span-2
{
    "title": "Return It Electronics NEG(.ai)",
    "filename": "Return It Electronics NEG.ai",
    "url": "returnit_neg.ai"
}
```






### Recycle NB Logo
```image
plain: true
span: 2
src: "recyclenb.png"
```

```download|span-2
{
    "title": "Recycle NB POS(.ai)",
    "filename": "Recycle NB POS.ai",
    "url": "recyclenb_colour.ai"
}
```

```download|span-2
{
    "title": "Recycle NB NEG(.ai)",
    "filename": "Recycle NB NEG.ai",
    "url": "recyclenb_neg.ai"
}
```
