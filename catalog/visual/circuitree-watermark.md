Key graphic elements are an essential part of our visual identity. The visuals have the ability to instantly tell a story about our brand. The following are Key visuals approved for use in all company materials and communications. These images cannot be modified in any way and must be used exactly as provided in the Resource files tab.


### Circuit-Tree Maple Leaf Watermark

In cases where the circuit-tree Maple Leaf Watermark is used as a background to text, please use this supplied version, available for download.
```image
plain: true
span: 3
src: "ctree.png"
```

```download|span-3
{
    "title": "RME Leaf Positive Watermark (.ai)",
    "filename": "RME Leaf Positive Watermark.ai",
    "url": "ctree.ai"
}
```





### Circuit-Tree Maple Leaf Watermark

Please find attached a negative white version of this element.
```image
plain: true
span: 3
src: "ctree_grey.png"
```

```download|span-3
{
    "title": "RME Leaf Watermark Negative White (.eps)",
    "filename": "RME Leaf Watermark Negative White.eps",
    "url": "ctree_grey.eps"
}
```
