In unifying the Recycle My Electronics brand nationally, regional and provincial specific imagery has been used to highlight the geographical identity of each province. By marrying province specific imagery with approved Recycle My Electronics “hero” images, each province has its own unique individual character while functioning within the overarching Recycle My Electronics brand.

### British Columbia - Tofino
```image
plain: true
span: 3
src: "bcbg.png"
```

```download|span-3
{
    "title": "BC Nature Background(.jpg)",
    "filename": "BC Nature Background.jpg",
    "url": "bcbg.zip"
}
```


### Manitoba - Whiteshell Provincial Park
```image
plain: true
span: 3
src: "mbbg.png"
```

```download|span-3
{
    "title": "MB Nature Background(.jpg)",
    "filename": "MB Nature Background.jpg",
    "url": "mbbg.zip"
}
```


### Newfoundland - Lobster Head Cove
```image
plain: true
span: 3
src: "nlbg.png"
```

```download|span-3
{
    "title": "NL Nature Background(.jpg)",
    "filename": "NL Nature Background.jpg",
    "url": "nlbg.zip"
}
```


### New Brunswick - Hopewell Rocks
```image
plain: true
span: 3
src: "nbbg.png"
```

```download|span-3
{
    "title": "NB Nature Background(.jpg)",
    "filename": "NB Nature Background.jpg",
    "url": "nbbg.zip"
}
```


### Nova Scotia - Cabot Trail
```image
plain: true
span: 3
src: "nsbg.png"
```

```download|span-3
{
    "title": "NS Nature Background(.jpg)",
    "filename": "NS Nature Background.jpg",
    "url": "nsbg.zip"
}
```



### Ontario - Bon Echo Provincial Park
```image
plain: true
span: 3
src: "onbg.png"
```

```download|span-3
{
    "title": "ON Nature Background(.jpg)",
    "filename": "ON Nature Background.jpg",
    "url": "onbg.zip"
}
```


### Prince Edward Island - Cavendish Beach
```image
plain: true
span: 3
src: "peibg.png"
```

```download|span-3
{
    "title": "PEI Nature Background(.jpg)",
    "filename": "PEI Nature Background.jpg",
    "url": "peibg.zip"
}
```


### Saskatchewan - Killdeer Badlands, Grasslands National Park
```image
plain: true
span: 3
src: "skbg.png"
```

```download|span-3
{
    "title": "SK Nature Background(.jpg)",
    "filename": "SK Nature Background.jpg",
    "url": "skbg.zip"
}
```
