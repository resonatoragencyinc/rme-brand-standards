
The Minimum size has been carefully established to ensure our logo is reproduced correctly in smaller sizes. At Minimum size, the logo is still clearly legible and provides a strong level of identification. When using a lower-quality printing technique (i.e. screenprinting), it is recommended that the logo be used in a larger size.


#### Minimum Size
The logo must never be used in a smaller size than the size identified on the image below. For printed applications (i.e. offset printing), the width of the Primary logotype should not be reduced less than 1-inch. The width of the Secondary Stacked option should not be reduced less than 0.75-inches, and the Secondary Horizontal option should not be reduced less than 1.5-inches in width. For screen applications (i.e. a website or banner), the width of the Primary logotype should not be reduced less than 70 pixels. The width of the Secondary Stacked option should not be reduced less than 53 pixels, and the Secondary Horizontal option should not be reduced less than 108 pixels in width.

```image
plain: true
span: 6
src: "size.png"
```
