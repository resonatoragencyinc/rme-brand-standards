The Clear space has been established to ensure logo visibility and impact. Maintaining the Clear space zone between the logo and other graphic elements such as type, images, other logos, etc. ensures that the logo always appears unobstructed and distinctly separate from any other graphic elements.

#### Staging and Spacing
When using the logo, allowing it to "breathe" gives it maximum impact. Wherever possible, allow even more space around the logo than required by Clear space. The Clear space is proportional and is based on the letter-height of the word, "RECYCLE" in the logo. The construction of Clear space is identified in the diagram below.



```image
plain: true
span: 6
src: "space.png"
```
