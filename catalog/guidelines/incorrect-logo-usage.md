To maintain consistency in the application, the logo must be used as provided in the Logo section under the Resource files tab. The logo must not be redrawn or altered in terms of its appearance, components, colors, proportions, or any other property.

Below are some examples of incorrect logo usage.


```image
plain: true
span: 6
src: "incorrect.png"
```
