The strength of the Recycle My Electronics brand lies to some degree in that it is itself a call to action. It puts the audience in mind of the desired behaviour. However, that behaviour needs to be supported with campaign messaging that provokes consideration and action.
```image
plain: true
span: 6
src: "inurhands.png"
```
•	The campaign messaging “The future is in your hands. Don’t let it go to waste.” draws attention and focus to both the device that the consumer can recycle, but also the potential within it, from a resource recovery perspective. This campaign messaging helps the consumer to understand that they have a clear choice, to make use of the materials in the device they have, which can be recycled and used in the creation of other devices.


•	This campaign messaging also functions as a double entendre in that, while the consumer has the future in their hands in terms of the device, they also have a responsibility in terms of how they want the future to be. It is literally in their hands to choose how they would like to see both their device and their natural environment in the future. “Don’t let it go to waste.” is a strong close to this message, referring both to the device and to the opportunity to change behaviour.


##### The Shift Away from “Extend Nature’s Warranty”
While the messaging of Extend Nature’s Warranty has merit, it lacks urgency and clarity. The extension of a warranty has no direct relation to behaviour and requires further investigation through body copy and support creative to be easily understood. This creates challenges in applications such as banner advertising, out of home and other instances where the viewer is not spending longer periods of time with the creative. Even using this as a piece of body copy doubles up on the required messaging and provides multiple directions that could confuse the audience. In terms of the previous visual identification with nature replaced by television static, the target demographic of millennial, largely raised in a digital environment, has no reference point for the television static alluded to in the creative application.
