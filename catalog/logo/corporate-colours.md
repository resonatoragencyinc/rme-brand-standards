Beyond the Recycle My Electronics logo, colour is the most recognizable aspect of our brand identity. Our brand colors reflect our bold, diverse community. Using appropriate colours is one of the easiest ways to ensure our materials reflect the nation-wide cohesiveness of the Recycle My Electronics brand.

### Primary colors
Specifications for reproduction of our Primary colors are shown in the image below. The colors are specified for offset printing on white paper (CMYK and Pantone) and for use on computer monitors (RGB). When reproducing the Company colors on a different material, always make sure the colors visually match approved colors.

```color
span: 3
name: "Recycle My Electronics Green"
value: "#61BB46"
```
```color
span: 3
name: "Recycle My Electronics Dark Green"
value: "#004E42"
```

```table
span: 6
rows:
  - Colour: Recycle My Electronics Green
    HEX: 61BB46
    CMYK: 65, 0, 100, 0
    Pantone: 368
    RGB: 97, 187, 70
  - Colour: Recycle My Electronics Dark Green
    HEX: 004E42
    CMYK: 92, 25, 70, 68
    Pantone: 3305
    RGB: 0, 78, 66
```

### Secondary Colour Palette
In addition to the Primary colors, a palette of complementary, secondary support colors has been developed. The support color palette is intended to be used in conjunction with the primary brand colors to add visual interest and graphic distinction.

```color-palette|horizontal
colors:
   - {value: "#CEDC00"}
   - {value: "#00AF9A"}
   - {value: "#E87722"}
   - {value: "#EC1C79"}
   - {value: "#63666A"}
```

```table
span: 6
rows:
  - Colour: Rio Grande 
    HEX: CADC00
    CMYK: 25, 0, 98, 0
    Pantone: 381
    RGB: 206, 220, 0
  - Colour: Persian Green
    HEX: 00AF94
    CMYK: 90, 0, 52, 0
    Pantone: 3275
    RGB: 0, 175, 154
  - Colour: Tango
    HEX: E87722
    CMYK: 0, 49, 85, 9
    Pantone: 158
    RGB: 232, 119, 34
  - Colour: Cerise Red
    HEX: E31C79
    CMYK: 0, 88, 47, 11
    Pantone: 213
    RGB: 227, 28, 121
  - Colour: Shuttle Gray
    HEX: 63666A
    CMYK: 40, 30, 20, 66
    Pantone: Cool Grey 10
    RGB: 99, 102, 106
```

### Tertiary Colour Palette

```color-palette|horizontal
colors:
   - {value: "#787121"}
   - {value: "#603D20"}
   - {value: "#FFB25B"}
   - {value: "#6C1D45"}
   - {value: "#B1B3B3"}
```

```table
span: 6
rows:
  - Colour: Pacifika
    HEX: 787121
    CMYK: 24, 14, 94, 55
    Pantone: 385
    RGB: 120, 113, 33
  - Colour: Pickled Bean
    HEX: 603D20
    CMYK: 16, 67, 100, 71
    Pantone: 161
    RGB: 96, 61, 32
  - Colour: Texas Rose
    HEX: FFB25B
    CMYK: 0, 30, 64, 0
    Pantone: 150
    RGB: 255, 178, 91
  - Colour: Tawny Port
    HEX: 6C1D45
    CMYK: 20, 100, 22, 61
    Pantone: 222
    RGB: 108, 29, 69
  - Colour: Edward
    HEX: B1B3B3
    CMYK: 13, 9, 10, 27
    Pantone: Cool Grey 5
    RGB: 177, 179, 179
```