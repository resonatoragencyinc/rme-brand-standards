
For the purposes of differentiating between services within the brand, brand identities have been created for operations and business to business sections of the business. Below you will find downloadable approved versions of the operations logo.




### Operations Logo

This downloadable Operations logo is approved for usage. There are numerous versions for use here including positive, negative and reversed.
```image
plain: true
span: 3
src: "op_logo.png"
```

```download|span-3
{
    "title": "RME Operations Logo Pack (.zip)",
    "filename": "RME Operations Logo Pack",
    "url": "op_logo.zip"
}
```
