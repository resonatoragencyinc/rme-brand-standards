For the purposes of differentiating between services within the brand, brand identities have been created for operations and business to business sections of the business. Below you will find downloadable approved versions of the business to business logo.



### Business to Business Logo

This downloadable business to business logo is approved for usage. There are numerous versions for use here including positive, negative and reversed.b2b
```image
plain: true
span: 3
src: "b2b_logo.png"
```

```download|span-3
{
    "title": "RME Business Logo Pack (.zip)",
    "filename": "RME Business Logo Pack",
    "url": "b2b_logo.zip"
}
```
