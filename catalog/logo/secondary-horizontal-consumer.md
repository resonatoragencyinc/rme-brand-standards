
Whenever possible, all materials and communications should be created using the Primary logo. In situations where the Primary logo will not fit, a Secondary logos can be used. Use the Resource files tab to download the color variation best suited for the given application. The logo must be used as provided. Please visit the Guidelines section to review proper and improper usage of the Secondary logo.


### Secondary Horizontal Usage Logo
Whenever possible, the Secondary logo should appear in its full-color version. In the Resource files tab you can download a version specific to both spot-color and four-color process printing, as well as for use on screen and in electronic documents.
```image
plain: true
span: 3
src: "secondary_horizontal.png"
```

```download|span-3
{
    "title": "RME Horiztonal Logo Pack (.zip)",
    "filename": "RME Horiztonal Logo Pack",
    "url": "secondary_horizontal.zip"
}
```




### One-Colour Usage Black Logo
The one-color solid version of the Secondary logo should only be used when the full-color Secondary logo cannot be applied. This is often the case with signage and merchandise.
```image
plain: true
span: 3
src: "secondary_horizontal_black.png"
```

```download|span-3
{
    "title": "RME Horizontal One Colour Back Logo Pack (.zip)",
    "filename": "RME Horiztonal One Colour Back Logo Pack",
    "url": "secondary_horizontal_black.zip"
}
```




### Secondary Negative Usage White Logo
When the logo is used on a contrasting background or image, it may be reversed out in white. Be sure that the legibility of the logo is not compromised by background elements.
```image
plain: true
span: 3
src: "secondary_horizontal_neg.png"
```

```download|span-3
{
    "title": "RME Horiztonal Negative White Logo Pack (.zip)",
    "filename": "RME Horiztonal Negative White Logo Logo Pack",
    "url": "secondary_horizontal_neg.zip"
}
```
