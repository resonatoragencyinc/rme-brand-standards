As a key component of our identity, the primary logo is one of its most visible parts and is the preferred version for all materials and communications. Use the Resource files tab to download the version of the logo that is best suited for the given application. The logo must be used as provided and cannot be altered in any way. Please visit the Guidelines section to review proper and improper usage of the logo.


### Primary Usage Logo
Whenever possible, the logo should appear in its full-color version. In the Resource files tab you can download a version specific to both spot-color and four-color process printing, as well as for use on screen and in electronic documents.
```image
plain: true
span: 3
src: "primary_logo.png"
```

```download|span-3
{
    "title": "RME Primary Usage Logo Pack(.zip)",
    "filename": "RME Primary Usage Logo Pack",
    "url": "primary_logo.zip"
}
```



### One-Colour Usage Black Logo
The one-color solid version of the logo should only be used when the full-color logo cannot be applied. This is often the case with signage and merchandise.
```image
plain: true
span: 3
src: "primary_logo_neg.png"
```

```download|span-3
{
    "title": "RME One-Colour Black Logo Pack(.zip)",
    "filename": "RME One-Colour Black Logo Pack",
    "url": "primary_logo_neg.zip"
}
```




### Primary Negative Usage White Logo
When the logo is used on a contrasting background or image, it may be reversed out in white. Be sure that the legibility of the logo is not compromised by background elements.
```image
plain: true
span: 3
src: "primary_logo_black.png"
```

```download|span-3
{
    "title": "RME Primary Negative White Logo Pack(.zip)",
    "filename": "RME Primary Negative White Logo Pack",
    "url": "primary_logo_black.zip"
}
```
