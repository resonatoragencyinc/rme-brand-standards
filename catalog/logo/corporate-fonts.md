The Corporate fonts are a fundamental part of our visual style that help achieve a unique and consistent look across our materials. The Primary fonts must be used on all printed materials and communications - and if possible, also on the website and online communication. Substitution fonts are provided for situations when Primary fonts are not available or suitable.

### Univers - Primary Corporate Typeface
Our Primary Font is Univers. The font is available in a wide range of weights which allow various typographic treatments, from bold headlines to easy-to-read body text. The Univers font can be purchased at https://www.fontshop.com/families/univers. We highly recommend purchasing the “Open type” version of the font because it may be used on both PC and Mac platforms.

```image
plain: true
span: 6
src: "corp_font.png"
```

### About the Univers Typeface
Univers was designed by Adrian Frutiger on Swiss principles for Charles Peignot at Deberny & Peignot. Frutiger imposed strict discipline across all elements of the series, from light to dark, extra condensed to extended, a concordance of design that was possible in the foundry type and photocomposition fonts. Any version may be mixed within a word with any other. It may be argued that the design of the most popular central series is limited by strict conformity to little used extremes. 

If Helvetica gives us the strongest central designs at some sacrifice in uniformity across the series, Univers gives us a uniform series by disciplining the central designs. Alteration of character widths required by the Monotype caster separates Monotype Univers from the original; the Linotype photocomposition version, designed by Frutiger, has a more even color across the series, achieved by relaxing the original rigid formula for stroke width.

```image
plain: true
span: 6
src: "1957.jpg"
```

### Alternate Type Attribute and Weight Options
Depending on the application, different type weights and attributes can create hierarchy and emphasis. See additional options of the Univers typeface that may be used appropriately below.

```image
plain: true
span: 3
src: "corp_font3.png"
```

```image
plain: true
span: 3
src: "corp_font4.png"
```

### Web and Digital Substitution Font Usage - Montserrat
Where the Primary font cannot be used (i.e. electronic documents such as Word or Powerpoint, or the internet), please use the Montserrat Type Family for headings and Open Sans for paragraph text instead. 

```type
{
  "headings": [48,30,25,21,15,14],
  "font": "Montserrat",
  "color": "#161922",
  "weight": 600
}
```

```download|span-6
{
    "title": "Montserrat",
    "subtitle": "Font Squirrel",
    "url": "https://www.fontsquirrel.com/fonts/montserrat"
}
```

```type
{
  "paragraphs": ["14"],
  "font": "Open Sans",
  "color": "#626262"
}
```

```download|span-6
{
    "title": "Open Sans",
    "subtitle": "Font Squirrel",
    "url": "https://www.fontsquirrel.com/fonts/open-sans"
}
```
