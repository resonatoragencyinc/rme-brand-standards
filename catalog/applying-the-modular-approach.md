### Applying the Modular Visual Approach to the National Campaign

This campaign approach for the brand can be customized across the country, with local customization of both target audience and geography, localized to retain the impact of recognizable provincial areas that help to highlight the impact of the desired recycling behaviour. In each case, the campaigns ask each audience to consider the future for their own, local, natural environment with the behaviour of the devices they own today. The impact of the statement “The future is in your hands. Don’t let it go to waste.” functions both literally in this case as it relates to their device, but also figuratively as it relates to the recognizable local geography and not letting this environment also go to waste. This online identity manual contains design guidelines for implementing our identity and visual style. On the pages listed on the left-hand side, you will find all the necessary information about the brand and the rules for using it. The manual is designed both for our own employees and our partners, such as ad agencies, graphic studios and designers, printing offices, and other company partners and suppliers.

##### Navigation
On the left-hand side of the screen there is a complete list of all the pages, which are sorted into several categories. The list acts as a navigation menu – simply click on a link to move to the given page.

##### Downloads
The logo and other components of our identity can be downloaded from the Resource files located next to the preview images. Files are provided in various file formats that are suitable for print and screen application.


##### Send By Email
To email a page from the manual, click on the arrow icon on the top right-hand side of any page. Fill in the form that pops up, enter your message and click the “Send” button. An active link to the relevant page is automatically emailed to the recipient.

##### Password Protection
Resource files in this online identity manual may be protected with a password. This means that only people that have the password will be able to download logos, images and other artwork files. Please visit the Contact details page to request a password and access to the Resource files.
