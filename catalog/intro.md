# The Recycle My Electronics National brand serves to unify creative campaigns and consumer-facing brands across the country for EPRA.


The strength of this consumer-facing campaign, from a visual perspective, is the ability for a variety of demographic and recognizable natural backgrounds to be treated in a modular approach. This allows the campaign to be easily customized for geographic and audience targeting.

