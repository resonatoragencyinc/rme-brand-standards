# Electronics Handling Fees - Provincial Brochures
These items, available by province, are featured here.


### British Columbia EHF Brochure

```image
plain: true
span: 3
src: "bc_ehf.png"
```

```download|span-3
{
    "title": "BC EHF Brochure PDF(.pdf)",
    "filename": "BC EHF",
    "url": "bc_ehf.pdf"
}
```


### Manitoba EHF Brochure

```image
plain: true
span: 3
src: "mb_ehf.png"
```

```download|span-3
{
    "title": "MB EHF Brochure PDF(.pdf)",
    "filename": "MB EHF",
    "url": "mb_ehf.pdf"
}
```



### New Brunswick EHF Brochure

```image
plain: true
span: 3
src: "nb_ehf.png"
```

```download|span-3
{
    "title": "NB EHF Brochure PDF(.pdf)",
    "filename": "NB EHF",
    "url": "nb_ehf.pdf"
}
```



### Newfoundland and Labrador EHF Brochure

```image
plain: true
span: 3
src: "nl_ehf.png"
```

```download|span-3
{
    "title": "NL EHF Brochure PDF(.pdf)",
    "filename": "NL EHF",
    "url": "nl_ehf.pdf"
}
```

### Nova Scotia EHF Brochure

```image
plain: true
span: 3
src: "ns_ehf.png"
```

```download|span-3
{
    "title": "NS EHF Brochure PDF(.pdf)",
    "filename": "NS EHF",
    "url": "ns_ehf.pdf"
}
```




### Ontario EHF Brochure
```image
plain: true
span: 3
src: "on_ehf.png"
```

```download|span-3
{
    "title": "ON EHF Brochure PDF(.pdf)",
    "filename": "ON EHF",
    "url": "on_ehf.pdf"
}
```




### Prince Edward Island EHF Brochure

```image
plain: true
span: 3
src: "pei_ehf.png"
```

```download|span-3
{
    "title": "PEI EHF Brochure PDF(.pdf)",
    "filename": "PEI EHF",
    "url": "pei_ehf.pdf"
}
```




### Quebec EHF Brochure
```image
plain: true
span: 3
src: "qc_ehf.png"
```

```download|span-3
{
    "title": "QC EHF Brochure PDF(.pdf)",
    "filename": "QC EHF",
    "url": "qc_ehf.pdf"
}
```




### Saskatchewan EHF Brochure

```image
plain: true
span: 3
src: "sas_ehf.png"
```

```download|span-3
{
    "title": SK EHF Brochure PDF(.pdf)",
    "filename": "SK EHF",
    "url": "sas_ehf.pdf"
}
```
