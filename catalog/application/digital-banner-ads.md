# Provincial Digital Banner Ads


These are the current downloadable provincial digital banner ads.

### British Columbia

```image
plain: true
span: 3
src: "bc_banner.png"
```

```download|span-3
{
    "title": "BC Banner.zip",
    "filename": "BC Banner",
    "url": "bc_banner.zip"
}
```



### Manitoba

```image
plain: true
span: 3
src: "mb_banner.png"
```

```download|span-3
{
    "title": "MB Banner.zip",
    "filename": "MB Banner",
    "url": "mb_banner.zip"
}
```



### New Brunswick

```image
plain: true
span: 3
src: "nb_banner.png"
```

```download|span-3
{
    "title": "NB Banner.zip",
    "filename": "NB Banner",
    "url": "nb_banner.zip"
}
```




### French

```image
plain: true
span: 3
src: "fr_banner.png"
```

```download|span-3
{
    "title": "FR Banner.zip",
    "filename": "FR Banner",
    "url": "fr_banner.zip"
}
```


### Newfoundland and Labrador

```image
plain: true
span: 3
src: "nl_banner.png"
```

```download|span-3
{
    "title": "NL Banner.zip",
    "filename": "NL Banner",
    "url": "nl_banner.zip"
}
```




### Nova Scotia

```image
plain: true
span: 3
src: "ns_banner.png"
```

```download|span-3
{
    "title": "NS Banner.zip",
    "filename": "NS Banner",
    "url": "ns_banner.zip"
}
```



### Ontario

```image
plain: true
span: 3
src: "on_banner.png"
```

```download|span-3
{
    "title": "ON Banner.zip",
    "filename": "ON Banner",
    "url": "on_banner.zip"
}
```


### Prince Edward Island

```image
plain: true
span: 3
src: "pei_banner.png"
```

```download|span-3
{
    "title": "PEI Banner.zip",
    "filename": "PEI Banner",
    "url": "pei_banner.zip"
}
```


### Saskatchewan

```image
plain: true
span: 3
src: "sas_banner.png"
```

```download|span-3
{
    "title": "SAS Banner.zip",
    "filename": "SAS Banner",
    "url": "sas_banner.zip"
}
```
