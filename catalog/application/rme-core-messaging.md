### Core Messaging - Recycle My Electronics
The future is in your hands. Don’t let it go to waste. The electronics we use every day are filled with reusable resources. By safely and securely recycling your end-of-life electronics you are helping the environment extend natural resources through responsible recovery and reuse. Out-of-use electronics are composed of reusable materials, like glass and plastic and precious metals, which can be recycled and put back into manufacturing. The Earth is reaching out for your help. To find out what and where to recycle, visit: recycleMYelectronics.ca

```image
plain: true
span: 6
src: "coremessaging.png"
```
