### Popular Media Q & A
Here you will find a templated Q & A for popular questions from and for media.

```image
plain: true
span: 3
src: "qanda.png"
```


```download|span-3
{
    "title": "RME Media Q and A(.pdf)",
    "filename": "RME MEdia Q and A",
    "url": "qanda.pdf"
}
```
