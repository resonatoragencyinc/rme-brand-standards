

*** In our communications across the country, we are striving for consistency of voice, tone and identity. That being said, for each region, we are providing slight modifications and differences to accommodate and highlight regional specificity. ***



### Directing Our Audience to Our Website
For each province, we are directing users to a unique provincial website URL. Should users go to recycleMYelectronics.ca, they will always have the option to choose their province but for specific applications in advertising and other communications, we direct to provincial website URL's.

```image
plain: true
span: 3
src: "Radiotop.png"
```




### Radio Advertising
These files provide 30 second radio advertising for each province as approved.

```image
plain: true
span: 3
src: "radiomic.png"
```


```download|span-3
{
    "title": "RME Radio Ads(.zip)",
    "filename": "RME Radio Ads",
    "url": "radio_national.zip"
}
```
