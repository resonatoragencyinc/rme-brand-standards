# Counter/Tent Cards


The provincial counter cards and tent cards are available here for download.


### British Columbia Counter Card

```image
plain: true
span: 3
src: "Bc_Counter_Card.png"
```

```download|span-3
{
    "title": "BC Counter Card Print PDF(.pdf)",
    "filename": "EPRA_Counter_Card",
    "url": "EPRA_BC_Counter_Card_2017_Print.pdf"
}
```






### Manitoba Counter Card
```image
plain: true
span: 3
src: "MB_Counter_Card.png"
```

```download|span-3
{
    "title": "MB Counter Card Print PDF(.pdf)",
    "filename": "EPRA_Counter_Card",
    "url": "EPRA_MB_Counter_Card_2017_Print.pdf"
}
```








### New Brunswick Tent Card
```image
plain: true
span: 3
src: "NB_Tent_Card.png"
```

```download|span-3
{
    "title": "NB Tent Card Print PDF(.pdf)",
    "filename": "EPRA_NB_Tent_Card",
    "url": "EPRA_NB_Tent_Card_2017_Print.pdf"
}
```







### Newfoundland and Labrador Tent Card
```image
plain: true
span: 3
src: "NFL_Tent_Card.png"
```

```download|span-3
{
    "title": "NL Tent Card Print PDF(.pdf)",
    "filename": "EPRA_NL_Tent_Card",
    "url": "EPRA_NL_Tent_Card_2017_Print.pdf"
}
```




### Nova Scotia Tent Card
```image
plain: true
span: 3
src: "NS_Tent_Card.png"
```

```download|span-3
{
    "title": "NS Tent Card Print PDF(.pdf)",
    "filename": "EPRA_NS_Tent_Card",
    "url": "EPRA_NS_Tent_Card_2017_Print.pdf"
}
```





### Prince Edward Island Tent Card
```image
plain: true
span: 3
src: "PEI_Tent_Card.png"
```

```download|span-3
{
    "title": "PEI Tent Card Print PDF(.pdf)",
    "filename": "EPRA_PEI_Tent_Card",
    "url": "EPRA_PEI_Tent_Card_2017_Print.pdf"
}
```





### Saskatchewan Counter Card
```image
plain: true
span: 3
src: "SAS_Counter_Card.png"
```

```download|span-3
{
    "title": "SK Counter Card Print PDF(.pdf)",
    "filename": "EPRA_SAS_Counter_Card",
    "url": "EPRA_SK_CounterCard_2017_Print.pdf"
}
```
