### Recycle My Electronics Boiler Plate
About Electronic Products Recycling Association:  As a recognized industry-led not-for-profit organization, Electronic Products Recycling Association (EPRA) provides environmental compliance programs for manufacturers, distributors and retailers of electronics. EPRA is responsible for implementing and operating, on behalf of their stewards, a safe and secure program for the recovery and reclamation of end-of-life electronic products. Who are we? Recycle My Electronics is your go-to resource for electronics recycling. We’re here to help you recycle your end-of-life electronic products and provide educational resources. The Recycle My Electronics program in Canada safely diverts more than 15.5 million devices from Canada’s landfills and illegal export every year. With 40% more drop off centres across Canada – most Canadian’s live within 25 km of a collection depot. It’s easy too! Our interactive postal code look up locates collection depots and events across the country for safe and secure recycling. To find out what and where to recycle, visit: recycleMYelectronics.ca To find out what and where to recycle, visit: recycleMYelectronics.ca

```image
plain: true
span: 6
src: "boilerplate.png"
```

